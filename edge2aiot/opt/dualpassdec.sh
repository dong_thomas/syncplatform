#!/bin/bash
sourcePath=$1
keyPath=$2
passPath=$3
targetPath=$4

pass=$(cat $passPath)

openssl rsautl -decrypt -inkey rsa/privkey-jp.pem -in $keyPath -out ${keyPath}.txt
openssl enc -d -aes-256-cbc -in $sourcePath -out ${sourcePath}.zip -pass file:${keyPath}.txt
7z x ${sourcePath}.zip -o$targetPath  -p$pass
