import os, sys, time
import pika
import json
import Geohash
import re
from influxdb import InfluxDBClient

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost',heartbeat=0))
channel = connection.channel()
channel.queue_declare(queue='hello')

#influxdb
host = "127.0.0.1"
port = 8086
user = "nchcuser0"
password = "nchcnchc"
database = "lassDB"
client = InfluxDBClient(host, port, user, password, database)  # 初始化

def data2influxdb(data):
    d = json.loads(data)
    temperature_data = float(0)
    topic = d['topic']
    if 'device_id' in d:
        device = d['device_id']
    else:
        return

    dateTime = "{0}T{1}Z".format(d['date'], d['time'])
    if 's_t0' in d:
        temperature_data = float(d['s_t0'])
    if 's_h0' in d:
        humidity_data = float(d['s_h0'])
    if 's_d0' in d:
        pm25_data = float(d['s_d0'])
    if 's_d1' in d:
        pm10_data = float(d['s_d1'])
    if 'gps_lat' in d:
        lat = float(d['gps_lat'])
    if 'gps_lon' in d:
        lon = float(d['gps_lon'])
    geohash_data = Geohash.encode(float(lat), float(lon))
    temperature_rec = [{'measurement': 'temperature',
        'tags': {'topic': topic, 'device' : device, 'geohash' : geohash_data},
        'fields': {'value':temperature_data},
        'time':dateTime
        }]
    #print(temperature_rec)
    client.write_points(temperature_rec)

    humidity_rec = [{'measurement': 'humidity',
        'tags': {'topic': topic, 'device' : device, 'geohash' : geohash_data},
        'fields': {'value':humidity_data},
        'time':dateTime
        }]
    client.write_points(humidity_rec)

    pm25_rec = [{'measurement': 'pm25',
        'tags': {'topic': topic, 'device' : device, 'geohash' : geohash_data},
        'fields': {'value':pm25_data},
        'time':dateTime
        }]
    client.write_points(pm25_rec)
    print(geohash_data)

    pm10_rec = [{'measurement': 'pm10',
        'tags': {'topic': topic, 'device' : device, 'geohash' : geohash_data},
        'fields': {'value':pm10_data},
        'time':dateTime
        }]
    client.write_points(pm10_rec)


def callback(ch, method, properties, body):
    try:
        data2influxdb(body)
    except:
        fo = open("faildata.txt", "w+")
        fo.write( str(body) )
        fo.close()
    print (" [x] Received %s", body)


channel.basic_consume(callback, queue='hello', no_ack=True)
channel.start_consuming()
