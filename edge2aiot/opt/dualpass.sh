#!/bin/bash
sourcePath=$1
passPath=$2
targetPath=$3

pass=$(cat $passPath)

7z a ${targetPath}/${sourcePath}.zip  $sourcePath -p${pass}
openssl rand -base64 64 > ${targetPath}/key.bin
openssl enc -aes-256-cbc -salt -in ${targetPath}/${sourcePath}.zip -out ${targetPath}/${sourcePath}.zip.enc -pass file:${targetPath}/key.bin
openssl rsautl -encrypt -inkey rsa/public_key_jp.pem -pubin -in ${targetPath}/key.bin -out ${targetPath}/key.bin.enc
#rm ${targetPath}/key.bin
#rm ${targetPath}/${sourcePath}.zip
